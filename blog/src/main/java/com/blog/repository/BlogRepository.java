package com.blog.repository;

import java.util.Map;

import com.blog.modal.BlogDetails;

public interface BlogRepository {
     public void createBlog(BlogDetails blogDetails);
 	 public void updateBlog(BlogDetails blogDetails);
 	 public BlogDetails getBlog(String blogId);
 	 public Map<String,BlogDetails> getAllBlogs();
}

