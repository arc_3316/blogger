package com.blog.modal;

import com.blog.enums.StatusType;

public class BlogDetails {

	private String blogId;
	
	private String userId;
	
	private String blogContent;
	
	private String blogTitle;
	
	private int vote;
	
	private StatusType status;

	public String getBlogId() {
		return blogId;
	}

	public void setBlogId(String blogId) {
		this.blogId = blogId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBlogContent() {
		return blogContent;
	}

	public void setBlogContent(String blogContent) {
		this.blogContent = blogContent;
	}

	public String getBlogTitle() {
		return blogTitle;
	}

	public void setBlogTitle(String blogTitle) {
		this.blogTitle = blogTitle;
	}

	public int getVote() {
		return vote;
	}

	public void setVote(int vote) {
		this.vote = vote;
	}

	public StatusType getStatus() {
		return status;
	}

	public void setStatus(StatusType status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "BlogDetails [blogId=" + blogId + ", userId=" + userId + ", blogContent=" + blogContent + ", blogTitle="
				+ blogTitle + ", vote=" + vote + ", status=" + status + "]";
	}
	
	
}
