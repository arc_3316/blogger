package com.blog.service;

import java.util.List;

import com.blog.modal.BlogDetails;

public interface SearchBlogService {
   public List<BlogDetails> getAllPublishedBlog();
   public List<BlogDetails> getAllUnPublishedBlog();
}
