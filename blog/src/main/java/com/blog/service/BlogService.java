package com.blog.service;

import com.blog.modal.BlogDetails;

public interface BlogService {

	public void createBlog(BlogDetails blogDetails);
	public BlogDetails archiveBlog(String blogId);
	public void updateBlog(BlogDetails blogDetails);
	public BlogDetails getBlog(String blogId); 
	
}
