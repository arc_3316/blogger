package com.blog.service;

import java.util.List;

import com.blog.modal.BlogDetails;

public interface NotificationService {
        public List<BlogDetails> fetchPendingBlogs();
        public List<BlogDetails> fetchRejectedBlogs();
        public List<BlogDetails> fetchInProgressBlogs();
}
