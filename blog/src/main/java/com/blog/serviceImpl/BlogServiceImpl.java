package com.blog.serviceImpl;

import java.util.Scanner;

import com.blog.enums.StatusType;
import com.blog.exception.BlogException;
import com.blog.modal.BlogDetails;
import com.blog.repository.BlogRepository;
import com.blog.repositoryImpl.BlogRepositoryImpl;
import com.blog.service.BlogService;

public class BlogServiceImpl implements BlogService {
	
	 private static Scanner scanner = new Scanner(System.in);

	/**
	 *  Creating dummy blog data (sample example)
	 *  @param blogDetails
	 */
	public void createBlog(BlogDetails blogDetails) {
           try {
        	   //In real senario user not need to enter any uuid by them selves.
               blogDetails.setUserId("1234");
               System.out.println("Enter blog Title:");
               blogDetails.setBlogTitle(scanner.nextLine());
               blogDetails.setStatus(StatusType.CONTAIN_REVIEW_PENDING);
               blogDetails.setVote(0);
               System.out.println("Enter blog content:");
               blogDetails.setBlogContent(scanner.nextLine());
               BlogRepository blogRepository = new BlogRepositoryImpl();
               blogRepository.createBlog(blogDetails);
           }catch(BlogException e) {
        	   System.out.println(e.getMessage());
           }
               
	}

	public BlogDetails archiveBlog(String blogId) {
        String userId = "1234";
        BlogRepository blogRepository = new BlogRepositoryImpl();
        BlogDetails blogDetails = blogRepository.getBlog(blogId);
        if(blogDetails != null && blogDetails.getUserId().equals(userId)) {
        	blogDetails.setStatus(StatusType.ARCHIVE);
        	blogRepository.updateBlog(blogDetails);
        	System.out.println("Not Authorized");
        }
        return blogDetails;
	}
	
	public void updateBlog(BlogDetails blogDetails) {
		// As of now we are updating only blog title.
		System.out.println("Please update blog title:");
		blogDetails.setBlogTitle(scanner.nextLine());
		BlogRepository blogRepository = new BlogRepositoryImpl();
		blogRepository.updateBlog(blogDetails);		
	}

	public BlogDetails getBlog(String blogId) {
		BlogRepository blogRepository = new BlogRepositoryImpl();
		return blogRepository.getBlog(blogId);
	}

}
