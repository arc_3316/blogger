package com.blog.serviceImpl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.blog.enums.StatusType;
import com.blog.modal.BlogDetails;
import com.blog.repository.BlogRepository;
import com.blog.repositoryImpl.BlogRepositoryImpl;
import com.blog.service.NotificationService;

public class NotificationServiceImpl implements NotificationService {

	BlogRepository blogRepository = new BlogRepositoryImpl();
	
	public List<BlogDetails> fetchPendingBlogs() {
		Map<String,BlogDetails> blogData = (Map<String, BlogDetails>) blogRepository.getAllBlogs();
		List filterPendingBlogs = blogData.values().stream().
												filter(blog -> blog.getStatus() == StatusType.APPROVAL_PENDING).
																collect(Collectors.toList());
		return filterPendingBlogs;
	}

	public List<BlogDetails> fetchRejectedBlogs() {
		Map<String,BlogDetails> blogData = (Map<String, BlogDetails>) blogRepository.getAllBlogs();
		List filterRejectedBlogs = blogData.values().stream().
												filter(blog -> blog.getStatus() == StatusType.REJECTED).
																collect(Collectors.toList());
		return filterRejectedBlogs;
	}

	public List<BlogDetails> fetchInProgressBlogs() {
		Map<String,BlogDetails> blogData = (Map<String, BlogDetails>) blogRepository.getAllBlogs();
		List filterInProgressBlogs = blogData.values().stream().
												filter(blog -> blog.getStatus() == StatusType.PROGRESS).
																collect(Collectors.toList());
		return filterInProgressBlogs;
	}

}
