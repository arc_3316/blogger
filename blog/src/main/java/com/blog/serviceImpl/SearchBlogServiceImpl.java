package com.blog.serviceImpl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.blog.enums.StatusType;
import com.blog.modal.BlogDetails;
import com.blog.repository.BlogRepository;
import com.blog.repositoryImpl.BlogRepositoryImpl;
import com.blog.service.SearchBlogService;

public class SearchBlogServiceImpl implements SearchBlogService{

	BlogRepository blogRepository = new BlogRepositoryImpl();
	
	public List<BlogDetails> getAllPublishedBlog() {
		Map<String,BlogDetails> blogData = (Map<String, BlogDetails>) blogRepository.getAllBlogs();
		List filterPublishedBlog = blogData.values().stream().
												filter(blog -> blog.getStatus() == StatusType.PUBLISH).
																collect(Collectors.toList());
		return filterPublishedBlog;
	}

	public List<BlogDetails> getAllUnPublishedBlog() {
		Map<String,BlogDetails> blogData = (Map<String, BlogDetails>) blogRepository.getAllBlogs();
		List filterUnPublishedBlog = blogData.values().stream().
												filter(blog -> blog.getStatus() != StatusType.PUBLISH).
																collect(Collectors.toList());
		return filterUnPublishedBlog;
	}

}
