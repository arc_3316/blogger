package com.blog.caller;

import java.util.Scanner;

import com.blog.modal.BlogDetails;
import com.blog.service.BlogService;
import com.blog.serviceImpl.BlogServiceImpl;
import com.blog.serviceImpl.SearchBlogServiceImpl;

public class CreateAndUpdateBlogCaller {

	public static void main(String[] args) {
		
		BlogService blogService = new BlogServiceImpl();
		Scanner sc = new Scanner(System.in);
		System.out.println("How many blogs you want to create?");
		int total_blog = sc.nextInt();
	    
		for(int i=0;i<total_blog;i++) {
			 BlogDetails newBlogDetails = new BlogDetails();
			 newBlogDetails.setBlogId(String.valueOf(i));
			 blogService.createBlog(newBlogDetails);
		}
		 
        String userId = "1234";
        
        System.out.println("Enter blog Id for blog update:");

        // real scenario blogId comes from the client side
        String blogId = sc.next();
        BlogDetails blogDetails = blogService.getBlog(blogId);
    
        if(blogDetails != null) {
        	if(blogDetails.getUserId().equals(userId)) {
            	blogService.updateBlog(blogDetails);
            }else {
            	System.out.println("Not Authorized");
            }
        }else {
        	System.out.println("Blog does not exits for blog Id: "+blogId);
        }
        
        System.out.println("Please enter blog Id to Archive:");
        String archiveBlogId = sc.next();
        BlogDetails blogDetailsArchive = blogService.archiveBlog(archiveBlogId);
        
        SearchBlogServiceImpl service = new SearchBlogServiceImpl();
        
        service.getAllUnPublishedBlog();
                
	}
}
