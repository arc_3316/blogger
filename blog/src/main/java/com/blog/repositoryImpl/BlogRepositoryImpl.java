package com.blog.repositoryImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.blog.enums.StatusType;
import com.blog.exception.BlogException;
import com.blog.modal.BlogDetails;
import com.blog.repository.BlogRepository;

public class BlogRepositoryImpl implements BlogRepository {
	
	private static Map<String,BlogDetails> blogDetailsData = new HashMap();

	/**
	 * Store blog in DataBase
	 * 	(As of now we are storing it in Map)
	 *  @param blogDetails
	 */
 	public void createBlog(BlogDetails blogDetails) {
		 try {
			 if(blogDetails != null) {
				 blogDetailsData.put(blogDetails.getBlogId(), blogDetails);
			 } 
		 }catch(Exception e) {
			 System.out.println(e.getMessage());
		 }
	}

	public void updateBlog(BlogDetails blogDetails) {
		try {
			 if(blogDetails != null) {
				 blogDetailsData.put(blogDetails.getBlogId(), blogDetails);
				 System.out.println("Blog update successfully: "+blogDetails);
			 } 
		 }catch(Exception e) {
			 System.out.println(e.getMessage());
		 }
	}
	
    /**
     * Get Blog by BlogId
     * 	As of now retrieving Blog from the Map 
     * 	@param blogId
     * 	@return blogDetails
     */
	public BlogDetails getBlog(String blogId) {
		BlogDetails blogDetails = null;
		try {
			blogDetails = blogDetailsData.get(blogId);
			if(blogDetails == null) {
				throw new BlogException();
			}
		}catch(BlogException e) {
			System.out.println(e.getMessage());
		}
		return blogDetails;
	}

	public Map<String,BlogDetails> getAllBlogs() {
		return blogDetailsData;
	}
}
