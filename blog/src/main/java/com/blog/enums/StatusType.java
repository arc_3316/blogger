package com.blog.enums;

public enum StatusType {
	
	APPROVAL_PENDING,
	FINAL_APPROVAL,
	ARCHIVE,
	PUBLISH,
	REJECTED,
	REVIEW,
	PROGRESS
	
}
